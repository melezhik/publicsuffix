use JSON::Fast; 

use TOML::Thumb;

my $META = from-json slurp 'META6.json';

my $CONFIG =  from-toml slurp 'dist.toml';

my $name   = $CONFIG<name> // $META<name>;

my $target = "$name-{ $META<version> }";

task-run ".tom/tasks/clean", %(
  META => $META,
  CONFIG => $CONFIG
);

task-run ".tom/tasks/check-workspace", %(
  META => $META,
);

task-run ".tom/tasks/check-changes", %(
  META => $META,
);

task-run ".tom/tasks/check-meta", %(
  META => $META,
  check-author => config()<check-author>,
);

task-run ".tom/tasks/test";

task-run ".tom/tasks/build", %(
  CONFIG => $CONFIG,
  META => $META, 
);

task-run ".tom/tasks/archive", %(
  name => $name,
  target => $target
);

task-run ".tom/tasks/release", %(
  CONFIG => $CONFIG,
  META => $META, 
);
