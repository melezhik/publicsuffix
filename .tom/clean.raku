use JSON::Fast; 

use TOML::Thumb;

my $META = from-json slurp 'META6.json';

my $CONFIG =  from-toml slurp 'dist.toml';

task-run ".tom/tasks/clean", %(
  META => $META,
  CONFIG => $CONFIG
);
