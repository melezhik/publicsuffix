use JSON::Fast; 

use TOML::Thumb;

my $META = from-json slurp 'META6.json';

my $CONFIG =  from-toml slurp 'dist.toml';

my $name   = $CONFIG<name> // $META<name>;
my $target = "$name-{ $META<version> }".IO;

task-run ".tom/tasks/clean", %(
  META => $META,
  CONFIG => $CONFIG
);

task-run ".tom/tasks/build", %(
  CONFIG => $CONFIG,
  META => $META, 
);
