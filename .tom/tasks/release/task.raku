my $NOW =  DateTime.now: formatter => {
    sprintf '%sT%s+%02d:00',
        .yyyy-mm-dd,
        .hh-mm-ss,
        .offset-in-hours.Int;
}

my $label = join ' - ', config()<META><version>, $NOW;
my $name  = config()<CONFIG><name> // config<META><name>;
my $build = "$name-{ config()<META><version> }".IO;

sub bump ( $next ) {
    for config()<META><provides>.values».IO {
        .spurt: .slurp.subst: ":ver<{config()<META><version>}>", ":ver<$next>";
        run < git add >, $_;
    }

    .spurt: .slurp.subst: config()<META><version>, $next given 'META6.json'.IO;
    run < git add META6.json >;
}

# Make release commit

$build.child('Changes').copy: '.'.IO.child('Changes');

run < git add Changes >;
run < git commit -m >, config()<CONFIG><bump><commit_message> // "Release v{config()<META><version>}";
run « git tag "v{config()<META><version>}" »;

# Make bump commit

.spurt: .slurp.subst( $label, '{{$NEXT}}' ~ "\n\n$label" ) given 'Changes'.IO;

bump %*ENV<NEXT_VERSION>
    // ( Version.new( config()<META><version> ).parts Z+ ( 0, 0, 1 ) ).join: '.';

run < git add Changes >;
run < git commit -m >, config()<CONFIG><bump><commit_message> // "Bump version";
