my $name = config()<CONFIG><name> // config()<META><name>;

use File::Find;
use File::Directory::Tree;

for sort find
    dir     => '.',
    name    => *.starts-with("$name-") {

    next unless .e;
    say "Removing $_";
    .d ?? .&rmtree !! .unlink;
}
