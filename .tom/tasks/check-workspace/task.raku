given config()<META> {
    my $name = "{ .<name> }-{ .<version> }";

    die "There is already a build directory for { .<name> }:ver<{ .<version> }>"
        if $name.IO.e;

    die "There is already a tarball for { .<name> }:ver<{ .<version> }> ($name.gz)"
        if "$name.gz".IO.e;
}

say 'Workspace looks good';
