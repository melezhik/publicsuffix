set -e

name=$(config name)

target=$(config target)

set -x

echo "Archiving as $target.tar.gz"

tar czf "$target.tar.gz" $target
