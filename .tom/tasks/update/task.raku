my $META = config()<META>;

my $date = 'resources/public_suffix_list.dat'.IO.modified.DateTime;

sub bump ( $next ) {
    for config()<META><provides>.values».IO {
        .spurt: .slurp.subst: ":ver<{config()<META><version>}>", ":ver<$next>";
        run < git add >, $_;
    }

    .spurt: .slurp.subst: config()<META><version>, $next given 'META6.json'.IO;
    run < git add META6.json >;
}

bump do {
    my @parts = $META<version>.split: '.';
    @parts.pop;
    @parts.push: $date.yyyy-mm-dd.subst( '-', :g );
    @parts.join: '.';
}

.spurt:
    .slurp.subst(
        '{{$NEXT}}' ~ "\n",
        '{{$NEXT}}' ~ "\n    - Update public suffix list\n"
    ) given 'Changes'.IO;

run < git add resources Changes >;
run « git commit -m "Update public suffix list" »;
