#!raku

use HTTP::Tiny;

%*ENV<HTTP_TINY_DEBUG> = 1;
my $url = config()<url>;

say "download {$url}";

given HTTP::Tiny.new {
    my $res = .mirror: $url, 'resources'.IO.child('public_suffix_list.dat');

    exit 0 unless $res<status> eq 200; # No changes

    .mirror:
        'https://raw.githubusercontent.com/publicsuffix/list/master/tests/tests.txt',
        'resources'.IO.child('tests.txt');
}
