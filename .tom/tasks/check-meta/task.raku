die 'Missing META6.json' unless 'META6.json'.IO.e;

given config()<META> {
    die "Bad version: { .<version> }"
        unless .<version> ~~ / ^ [ \d+ ] ** 3 % '.' $ /;

    if config()<check-author> {
      die "Bad auth: '{ .<auth> }' should be zef:$*USER"
         unless .<auth> eq "zef:$*USER";
    }

    for .<provides>.kv -> $key, $value {
        for $value».IO -> $path {
            next unless $path.slurp ~~ / ':ver<' $<version> = ( <-[>]>+ ) '>' /;

            die "Version in $path ({ $<version> }) does not match the one in META ({ .<version> })"
                unless ~$<version> eq .<version>;
        }

        use lib '.';
        my $meta = $_;
        die "Could not load {$key}:ver<{ .<version> }>:auth<{ .<auth> }>"
            unless $*REPO.repo-chain
                .map({ .?candidates( $key, ver => $meta<version>, auth => $meta<auth> ) })
                .flat
                .first: *.defined;
    }

    die "Missing resource from META: $_" unless 'resources'.IO.child($_).e
        for .<resources>.List;

    with .<source-url> {
        die .err.slurp if .exitcode
            given run « git ls-remote $_ », :out, :err;
    }
}

say 'META looks good';
