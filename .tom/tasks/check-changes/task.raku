say 'Checking changes';

die 'No Changes file' unless 'Changes'.IO.e;

given 'Changes'.IO.slurp {
    die 'Change log does not have {{$NEXT}} placeholder'
        when !/ ^^ '{{$NEXT}}' $$ /;
}

say 'Changes look good';
