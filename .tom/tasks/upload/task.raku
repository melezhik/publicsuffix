if config()<dry-run> {
  say "dry-run is enabled, exiting ...";
  exit(0)
}

my $META = config()<META>;

my $tarball = "{ $META<name> }-{ $META<version> }.tar.gz".IO;

die "No tarball to upload!" unless $tarball.e;

if %*ENV<PUBLIC_SUFFIX_AUTORELEASE> {
    run < fez upload >, "--file=$tarball";
}
else {
    say "All set to release { $META<name> } v{ $META<version> } with";
    say "    zef upload --file=$tarball";
    say '';
    say "Not doing it myself because you didn't set PUBLIC_SUFFIX_AUTORELEASE";
}
