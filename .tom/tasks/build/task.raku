use File::Ignore;

use File::Find;

my $name   = config<CONFIG><name> // config()<META><name>;

my $target = "$name-{ config()<META><version> }".IO;

my $NOW =  DateTime.now: formatter => {
    sprintf '%sT%s+%02d:00',
        .yyyy-mm-dd,
        .hh-mm-ss,
        .offset-in-hours.Int;
}

$target.mkdir;

my $ignore = File::Ignore.new: rules => config()<CONFIG><build><exclude>;

for sort { .f, .chars },
    find dir => '.', exclude => *.basename.starts-with: '.' {

    when .d {
        # Ignore empty directories
        next unless .dir: test => { not .starts-with('.') }

        # Ignore if on exclude list
        next if $ignore.ignore-directory: $_;

        $target.child($_).mkdir;
    }

    when .f {
        # Ignore if on exclude list
        next if $ignore.ignore-file: $_;

        .copy: $target.child($_);
    }
}

my $label = join ' - ', config()<META><version>, $NOW;

.spurt: .slurp.subst( '{{$NEXT}}', "$label\n" )
    given $target.child: 'Changes';
