use JSON::Fast; 

my $META = from-json slurp 'META6.json';

task-run ".tom/tasks/check-workspace", %(
  META => $META,
);

task-run ".tom/tasks/check-changes", %(
  META => $META,
);

task-run ".tom/tasks/check-meta", %(
  META => $META,
  check-author => config()<check-author>
);

