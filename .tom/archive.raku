use JSON::Fast; 

use TOML::Thumb;

my $META = from-json slurp 'META6.json';

my $CONFIG =  from-toml slurp 'dist.toml';

my $name   = $CONFIG<name> // $META<name>;

my $target = "$name-{ $META<version> }";

task-run ".tom/tasks/build", %(
  CONFIG => $CONFIG,
  META => $META, 
);

task-run ".tom/tasks/archive", %(
  name => $name,
  target => $target
);
