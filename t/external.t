#!/usr/bin/env raku

use Test;
use PublicSuffix;

# From https://raw.githubusercontent.com/publicsuffix/list/master/tests/tests.txt
for 'resources/tests.txt'.IO.lines {
    next if .not or .starts-with: '//';

    my ( $have, $want ) = .split: ' ';

    my $label = "$have -> $want";

    $have = Str if $have eq 'null';
    $want = Str if $want eq 'null';

    CATCH {
        when X::PublicSuffix::BadDomain {
            .rethrow if $want.defined;
            throws-like { .rethrow },
                X::PublicSuffix::BadDomain, message => /': it must'/;
        }
    }

    is $have.&registrable-domain, $want, $label;
}

# From https://url.spec.whatwg.org/#host-miscellaneous
for <
    com                                       com          null
    example.com                               com          example.com
    www.example.com                           com          example.com
    sub.www.example.com                       com          example.com
    EXAMPLE.COM                               com          example.com
    github.io                                 github.io    null
    whatwg.github.io                          github.io    whatwg.github.io
    xn--kgbechtv                              xn--kgbechtv null
    example.إختبار                            إختبار       example.إختبار
    sub.example.إختبار                        إختبار       example.إختبار
    192.168.0.100                             null         null
    [2001:0db8:85a3:0000:0000:8a2e:0370:7334] null         null
     2001:0db8:85a3:0000:0000:8a2e:0370:7334  null         null
> -> $host, $pub, $reg {
    is $host.&registrable-domain, $reg eq 'null' ?? Str !! $reg, "registrable domain for $host";
    is $host.&public-suffix,      $pub eq 'null' ?? Str !! $pub, "public suffix for $host";
}

done-testing;
